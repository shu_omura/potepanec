module Potepan::ProductDecorator
  def self.prepended(base)
    base.class_eval do
      scope :with_price_and_images, -> { includes(master: %i(default_price images)) }
      scope :related_products, -> (product) { in_taxons(product.taxons).distinct.where.not(id: product.id) }
    end
  end

  Spree::Product.prepend self
end
