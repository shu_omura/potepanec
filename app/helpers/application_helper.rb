module ApplicationHelper
  def full_title(title)
    title.present? ? "#{title} - #{SITE_TITLE}" : SITE_TITLE
  end
end
