module Potepan
  class CategoriesController < ApplicationController
    def show
      @categories = Spree::Taxon.roots
      @taxon = Spree::Taxon.find(params[:id])
      @products = @taxon.all_products.with_price_and_images
    end

    def index
      @categories = Spree::Taxon.roots
      @products = Spree::Product.with_price_and_images.all
    end
  end
end
