module Potepan
  class ProductsController < ApplicationController
    def show
      @product = Spree::Product.find(params[:id])
      @related_products = Spree::Product.related_products(@product).
        with_price_and_images.
        sample(MAX_DISPLAY_PRODUCTS_COUNT)
    end
  end
end
