require 'rails_helper'

RSpec.describe ApplicationHelper do
  it 'displays title' do
    expect(full_title('')).to eq "#{SITE_TITLE}"
  end

  it 'displays title with arguments' do
    expect(full_title('shop')).to eq "shop - #{SITE_TITLE}"
  end

  it 'displays title with nil arguments' do
    expect(full_title(nil)).to eq "#{SITE_TITLE}"
  end
end
