RSpec.shared_context 'related_product setup' do
  let(:image_2) { create(:image, attachment: Rails.root.join('spec', 'factories', 'sample1.jpeg').open) }
  let(:related_product) { create(:product, taxons: [taxon]) }
  let!(:variant_2) { create(:variant, product: related_product, images: [image_2]) }
end
