RSpec.shared_context 'product setup' do
  let(:taxon) { create(:taxon) }
  let(:image) { create(:image) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:variant) { create(:variant, product: product, images: [image]) }
end
