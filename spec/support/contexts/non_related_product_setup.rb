RSpec.shared_context 'non_related_product setup' do
  let(:taxon_2) { create(:taxon, name: 'Foods') }
  let(:image_3) { create(:image, attachment: Rails.root.join('spec', 'factories', 'sample2.jpeg').open) }
  let(:non_related_product) { create(:product, taxons: [taxon_2]) }
  let!(:variant_2) { create(:variant, product: non_related_product, images: [image_2]) }
end
