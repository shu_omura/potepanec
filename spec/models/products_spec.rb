require 'rails_helper'

RSpec.feature 'Products', type: :model do
  include_context 'product setup'
  include_context 'related_product setup'
  include_context 'non_related_product setup'

  subject { Spree::Product.related_products(product) }

  it 'has related_products' do
    is_expected.to include related_product
  end

  it 'does not have non_related_products' do
    is_expected.not_to include non_related_product
  end
end
