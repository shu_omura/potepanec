require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  include_context 'product setup'
  let(:taxon_2) { create(:taxon, name: 'Foods') }
  let(:image_2) { create(:image, attachment: Rails.root.join('spec', 'factories', 'sample1.jpeg').open) }
  let(:child_taxon) { create(:taxon, name: 'Rice', parent_id: taxon_2.id) }
  let(:child_product) { create(:product, price: 99.99, taxons: [child_taxon]) }
  let!(:child_variant) { create(:variant, product: child_product, images: [image_2]) }

  describe 'categories and products interface' do
    context 'user visits #show and moves to products"index' do
      before { visit potepan_category_path(taxon.id) }

      it 'finds taxons and products' do
        expect(page).to have_title "#{taxon.name} - BIGBAG Store"
        expect_attributes_for(product)
        within('.breadcrumb') { expect_breadcrumbs(taxon.name) }
        within '.category-bar' do
          expect(page).to have_content taxon.name
          expect(page).to have_content taxon.root.name
          expect(page).to have_content taxon.products.size
        end

        click_link product.name

        expect(current_path).to eq potepan_product_path(product.id)
      end
    end

    context 'user visits products#show and moves to #index' do
      before { visit potepan_product_path(product.id) }

      it 'finds taxons and products' do
        expect(page).to have_title "#{product.name} - BIGBAG Store"
        expect_attributes_for(product)
        within('.breadcrumb') { expect_breadcrumbs(taxon.name, product.name) }

        find('#jump-to-categories').click

        expect(current_path).to eq potepan_categories_path
        expect(page).to have_title "Shop - BIGBAG Store"
        within('.breadcrumb') { expect_breadcrumbs }
        expect_attributes_for(product)
      end
    end

    context 'user clicks category names' do
      before { visit potepan_category_path(taxon.id) }

      it 'finds taxons and display products recources' do
        expect_not_attributes_for(child_product)

        click_link child_taxon.name

        expect_attributes_for(child_product)
      end
    end
  end

  def expect_breadcrumbs(*args)
    args.each { |arg| expect(page).to have_content arg }
    expect(page).to have_content 'Shop'
    expect(page).to have_link 'Home'
  end

  def expect_attributes_for(product)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_css "img[src='#{product.gallery.images.first.attachment(:large)}']"
  end

  def expect_not_attributes_for(product)
    expect(page).not_to have_content product.name
    expect(page).not_to have_content product.display_price
    expect(page).not_to have_css "img[src='#{product.gallery.images.first.attachment(:large)}']"
  end
end
