require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  include_context 'product setup'

  before { visit potepan_product_path(product.id) }

  it 'finds products and moves to /potepan' do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    within '.singleProduct' do
      expect_attributes_for(product)
      expect_additional_attributes_for(product)
    end
    within '.breadcrumb' do
      expect_breadcrumbs(product)
      click_link 'Home'
    end

    expect(current_path).to eq potepan_path
    expect(page).to have_title 'BIGBAG Store'
  end

  context 'with related_products' do
    include_context 'related_product setup'

    before { visit potepan_product_path(product.id) }

    it 'finds related_products, clicks and moves to categories#index' do
      within '.related-products' do
        expect_attributes_for(related_product)
        click_link related_product.name
      end

      expect(current_path).to eq potepan_product_path(related_product.id)
      within '.singleProduct' do
        expect_attributes_for(related_product)
        expect_additional_attributes_for(related_product)
      end
      within('.breadcrumb') { expect_breadcrumbs(related_product) }

      find('#jump-to-categories').click

      expect(current_path).to eq potepan_categories_path
      expect_attributes_for(product)
      expect_attributes_for(related_product)
    end

    context 'with more than 4 related_products' do
      let!(:related_products) do
        create_list(:product, 5, taxons: [taxon]).each do |item|
          create(:variant, product: item, images: [create(:image)])
        end
      end

      before { visit potepan_product_path(product.id) }

      it 'finds related_products only 4' do
        within('.related-products') { expect(page).to have_selector('a', count: 4) }
      end
    end
  end

  def expect_attributes_for(product)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_css "img[src='#{product.gallery.images.first.attachment(:large)}']"
  end

  def expect_additional_attributes_for(product)
    expect(page).to have_content product.description
    expect(page).to have_css "img[src='#{product.gallery.images.first.attachment(:small)}']"
  end

  def expect_breadcrumbs(product)
    expect(page).to have_content product.name
    expect(page).to have_link product.taxons.first.name
    expect(page).to have_link 'Shop'
    expect(page).to have_link 'Home'
  end
end
