require 'rails_helper'

RSpec.describe 'TopPage', type: :request do
  describe 'GET #index' do
    it 'returns http 200' do
      get potepan_path
      expect(response).to have_http_status(200)
    end
  end
end
