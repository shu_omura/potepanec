require 'rails_helper'

RSpec.describe 'Categories', type: :request do
  include_context 'product setup'
  let(:categories) { Spree::Taxon.roots }

  before { raise if categories.empty? }

  describe 'GET #show' do
    before { get potepan_category_path(taxon.id) }

    it 'returns http 200' do
      expect(response).to have_http_status(200)
    end

    it 'shows taxons' do
      categories.each { |category| expect(response.body).to include category.name }
      expect(response.body).to include taxon.name
    end

    it 'shows products' do
      expect(response.body).to include product.name
    end
  end

  describe 'GET #index' do
    before { get potepan_categories_path }

    it 'returns http 200' do
      expect(response).to have_http_status(200)
    end

    it 'shows taxons' do
      categories.each { |category| expect(response.body).to include category.name }
      expect(response.body).to include taxon.name
    end

    it 'shows products' do
      expect(response.body).to include product.name
    end
  end
end
