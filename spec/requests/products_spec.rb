require 'rails_helper'

RSpec.describe 'Products', type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:image) { create(:image) }
    let(:product) { create(:product, taxons: [taxon], deleted_at: deleted_at) }
    let!(:variant) { create(:variant, product: product, images: [image]) }

    context 'with an available product' do
      let(:deleted_at) { nil }

      before { get potepan_product_path(product.id) }

      it 'returns http 200' do
        expect(response).to have_http_status(200)
      end

      it 'shows products' do
        expect(response.body).to include product.name
        expect(response.body).to include image.attachment_file_name
      end

      context 'with related_products' do
        include_context 'related_product setup'

        before { get potepan_product_path(product.id) }

        it 'shows products' do
          expect(response.body).to include related_product.name
          expect(response.body).to include image_2.attachment_file_name
        end
      end
    end

    context 'with a deleted product' do
      let(:deleted_at) { Time.current }

      it 'returns exception' do
        expect do
          get potepan_product_path(product.id)
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
