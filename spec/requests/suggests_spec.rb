require 'rails_helper'

RSpec.feature 'Suggests', type: :request do
  describe '#index', vcr: true do
    before { get potepan_suggests_path, params: { keyword: keyword }, xhr: true }

    context 'with matched keyword params' do
      let(:keyword) { 'r' }

      it 'returns http 200' do
        expect(response.status).to eq 200
      end

      it 'returns suggests' do
        res = JSON.parse(response.body)
        expect(res.present?).to be_truthy
      end

      it 'returns suggests start with keyword' do
        JSON.parse(response.body).each do |str|
          expect(str.start_with?(keyword)).to be_truthy
        end
      end

      context 'with max_num params' do
        subject { JSON.parse(response.body).size }

        context 'default(=5)' do
          it 'returns suggest only 5' do
            is_expected.to eq 5
          end
        end

        context '=3' do
          before do
            stub_const('MAX_SUGGESTS_COUNT', 3)
            get potepan_suggests_path, params: { keyword: keyword }, xhr: true
          end

          it 'returns suggest only 3' do
            is_expected.to eq 3
          end
        end
      end
    end

    context 'with non matched keyword params' do
      let(:keyword) { 'hoge' }

      it 'returns http 200' do
        expect(response.status).to eq 200
      end

      it 'shows no suggests' do
        res = JSON.parse(response.body)
        expect(res.present?).to be_falsey
      end
    end

    context 'with invalid keyword params' do
      let(:keyword) { nil }

      it 'returns http 400' do
        expect(response.status).to eq 400
      end

      it 'includes error messages' do
        res = JSON.parse(response.body)
        expect(res['message']).to include '不正なパラメーターです'
      end
    end
  end
end
